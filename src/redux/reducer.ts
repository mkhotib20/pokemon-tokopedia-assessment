import { AnyAction } from "redux"
import { HYDRATE } from "next-redux-wrapper"
import { DECREMENT_POKEMON, DIRECT_POKEMON_COUNT, INCREMENT_POKEMON, RELEASE_POKEMON, SAVE_POKEMON } from "./constants"
import { Pokemon } from "interfaces/graphql"
import { saveMetaCount, saveToStorage } from "utils/pokemon"



export interface State {
	pokemonCount?: number
	metaCount?: any
	pokedex?: Pokemon[]
}

export const InitialState: State = {
	pokemonCount: 0,
	metaCount: {},
	pokedex: []
}

export const reducer = (state: State = InitialState, action: AnyAction) => {
	let pokedex = state.pokedex
	let metaData = {}
	switch (action.type) {
		case HYDRATE:
			if (action.payload.pokemonCount == 0) delete action.payload.pokemonCount
			if (action.payload.pokedex && action.payload.pokedex.length == 0) delete action.payload.pokedex

			return { ...state, ...action.payload }
			break
		case SAVE_POKEMON:
			if (Array.isArray(action.payload)) {
				pokedex = action.payload
			} else {
				pokedex.push(action.payload)
			}
			saveToStorage(pokedex)
			try {
				let countThisName = pokedex.filter(e => e.name==action.payload.name)
				metaData[action.payload.name] = countThisName.length
				metaData = saveMetaCount(metaData)
			} catch (error) {}
			return { ...state, pokedex:  pokedex, pokemonCount: pokedex.length, metaCount: metaData}
			break

		case RELEASE_POKEMON:
			let idxToRemove = pokedex.findIndex(p => p.nick_name == action.payload.nick_name)
			console.log("idxToRemove");
			console.log(idxToRemove);
			
			pokedex.splice(idxToRemove, 1)
			saveToStorage(pokedex)
			try {
				let countThisName = pokedex.filter(e => e.name==action.payload.name)
				metaData[action.payload.name] = countThisName.length
				metaData = saveMetaCount(metaData)
			} catch (error) {}
			return { ...state, pokedex: state.pokedex, pokemonCount: pokedex.length, metaCount: metaData}
			break

		case DIRECT_POKEMON_COUNT:
			return { ...state, pokemonCount:  action.payload}
			break

		case INCREMENT_POKEMON:
			return { ...state, pokemonCount:  state.pokemonCount+1}
			break

		case DECREMENT_POKEMON:
			return { ...state, pokemonCount:  state.pokemonCount-1}
			break

		default:
			return state
			break
	}
}