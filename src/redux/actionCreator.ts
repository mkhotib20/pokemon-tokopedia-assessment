import { Pokemon } from "interfaces/graphql";
import {Pokemon as Pokedex } from 'interfaces/index'
import { AnyAction } from "redux";
import { DIRECT_POKEMON_COUNT, INCREMENT_POKEMON, RELEASE_POKEMON, SAVE_POKEMON } from "./constants";


export const addPokemon = (newPokemon: Pokemon | Pokemon[]): AnyAction => {
    return {
        type: SAVE_POKEMON,
        payload: newPokemon
    }
}

export const releasePokemon = (pokemonTORelease: Pokedex): AnyAction => {
    return {
        type: RELEASE_POKEMON,
        payload: pokemonTORelease
    }
}

export const incrementPokemon = (): AnyAction => {
    return {
        type: INCREMENT_POKEMON
    }
}


export const decrementPokemon = (): AnyAction => {
    return {
        type: INCREMENT_POKEMON
    }
}



export const directPokemonCount = (num: number): AnyAction => {
    return {
        type: DIRECT_POKEMON_COUNT,
        payload: num
    }
}