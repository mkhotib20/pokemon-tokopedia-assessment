import SinglePokemon from 'components/general/SinglePokemon'

import Template from 'components/Template'
import { GqlQuery, PageProps,Pokemon } from 'interfaces'
import * as gql from 'gql-query-builder'	
import React, { Component, createRef, RefObject } from 'react'
import { Col, Row, Spinner } from 'reactstrap'
import { isBottom, words } from 'utils'
import { withHoc } from 'utils/hoc'
import { getFromStorage } from 'utils/pokemon'
import Alert from 'components/general/Alert'
import CatchingPokemon from 'components/general/CatchingPokemon'
import { releasePokemon } from 'redux/actionCreator'
import { PlainPokeball } from 'components/icons'


interface OwnedPokemonState {
	page: number
	pokemons: Pokemon[]
	catching: boolean
	selectedPokemon: Pokemon | null
}

export class OwnedPokemon extends Component<PageProps, OwnedPokemonState> {
	private popupAlert: RefObject<Alert>

	constructor(props:PageProps) {
		super(props)
		this.popupAlert = createRef()
		this.state = {
			selectedPokemon: null,
            catching: false,
			page: 1,
			pokemons: [],
		}
	}
	dataSize = () =>{
		return window.innerWidth <768 ? 9 : 11
	}
	query = (): GqlQuery => {
		let offset = (this.state.page-1)* this.dataSize()
		const query = gql.query({
			operation: 'pokemons',
			variables: {
				limit: {value: this.dataSize(), required: false},
				offset: offset
			},
			fields: [
				{
					results: ["name", "id", "image"]
				}
			]
		})		
		return query
	}
	async componentDidMount(){
		this.getPokemons(true)
	}

	getPokemons = async (force?: boolean) => {
		let poke = getFromStorage()
		let pokemonList: Pokemon[] = poke.map(v => ({
			id: v.id,
			image: v.sprites.front_default, 
			name: v.name,
			nick_name: v.nick_name
		}))
		this.setState({pokemons: pokemonList})
	}
	
	openPokemon = (data: Pokemon) => {
		this.setState({
			selectedPokemon: data
		}, () => this.popupAlert.current.open(`${words(data.nick_name)}
pokemon : ${data.name}
		`))
	}
	releasePokemon = () => {
		let {selectedPokemon} = this.state
		if (selectedPokemon!=null) {
			let {dispatch} = this.props
			this.setState({catching: true}, () => {
				this.popupAlert.current.close()
				setTimeout(() => {
					dispatch(releasePokemon(selectedPokemon))
					this.setState({catching: false}, this.getPokemons)
				}, 3000);
			})
		}
	}
	render() {
		return (
			<Template {...this.props}>
				<Col md="12" className="my-3">
					<Row>
					{this.state.pokemons.map((v,i) => {
						return(
							<Col key={i} xs="6" md="3">
								<SinglePokemon onClick={this.openPokemon} data={v} />
							</Col>
						)
					})}
					</Row>
				</Col>
				{this.state.pokemons.length==0 && (
					<Col md="12" className="mt-5 my-3">
						<PlainPokeball className="spin-and-left-toright" color="#dbdbdb" size={100} />
						<h6 className="text-muted text-center mt-3 text-bold">Nothing to show :( </h6>
					</Col>
				)}
				<Alert buttons={[
					{label: "Release", onClick: this.releasePokemon}
				]} ref={this.popupAlert} />

				{this.state.catching && <CatchingPokemon title="Releasing Pokemon . . ."  />}
			</Template>
		)
	}
}

export default withHoc(OwnedPokemon)
