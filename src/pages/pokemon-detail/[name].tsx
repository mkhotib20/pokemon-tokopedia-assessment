import Template from 'components/Template'
import { query } from 'gql-query-builder'
import { GqlQuery, PageProps } from 'interfaces'
import { Pokemon } from 'interfaces/graphql'
import { getter } from 'network'
import React, { Component, createRef, RefObject } from 'react'
import { Badge, Col } from 'reactstrap'
import {FaChevronLeft} from 'react-icons/fa'
import Link from 'next/link'
import Skeleton from 'react-loading-skeleton'
import { timeToCatch, words } from 'utils'
import PokemonTab from 'components/general/PokemonTab'
import SingleStats from 'components/general/SingleStats'
import { withHoc } from 'utils/hoc'
import { addPokemon } from 'redux/actionCreator'
import { PlainPokeball } from 'components/icons'
import CatchingPokemon from 'components/general/CatchingPokemon'
import Alert from 'components/general/Alert'
import { isNickNameExist } from 'utils/pokemon'

interface PokemonDetailState {
    pokemon?: Pokemon
    imgHeight: number
    viewOn: "moves" | "stats" | "overview"
    catching: boolean
}

interface PokemonDetailPageProps extends PageProps {

}

const pokemonDetails = ["name", "height", "weight", "base_experience"]

class PokemonDetail extends Component<PokemonDetailPageProps, PokemonDetailState> {

    private popupAlert: RefObject<Alert>
    private popupPrompt: RefObject<Alert>

    constructor(props: PokemonDetailPageProps) {
        super(props)
        this.popupAlert = createRef()
        this.popupPrompt = createRef()
        this.state = {
            viewOn: "overview",
            pokemon: null,
            catching: false,
            imgHeight: 200
        }
    }
    async componentDidMount(){

		let {data, error} = await getter<Pokemon>(this.query(), [],"pokemon")
		if (error!=null) {
			console.log(error)
		} else {
            this.setState({pokemon: data})  
		}
    }
    
	query = (): GqlQuery =>{
		return query({
            operation: "pokemon",
            fields: [
                ...pokemonDetails, "id",
                {sprites: ["front_default"]},
                {stats: ["base_stat", "effort", {stat: ["name"]}]},
                {types: [ {type: ["name"]} ]},
                {moves: [ {move: ["name"]} ]},

            ],
            variables: {
                name: {value: this.props.query?.name, required: true}
            }
        })
    }
    renderPartial = () =>{
        let {pokemon} = this.state
        switch (this.state.viewOn) {
            case "stats":
                return(
                    <Col md="8" className="offset-md-2 my-2 pokemon-sub-detail">
                        <table className="w-100 px-4">
                            <tbody>
                                {pokemon?.stats.map((v,i) => <SingleStats value={v.base_stat} label={words(v.stat.name)}  key={i}/>)}
                            </tbody>
                        </table>
                    </Col>
                )        
                case "overview":
                    return(
                        <Col md="8" className="px-4 offset-md-2 my-2 pokemon-sub-detail">
                            <table className="pokemon-overview w-100 px-4">
                                <tbody>
                                    {pokemonDetails.map((v,i) => (
                                        <tr>
                                            <td>{words(v)}</td>
                                            <td>{pokemon!=null ? words(pokemon[v]): <Skeleton/>}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </Col>
                    )    
                case "moves":
                    return(
                        <Col md="8" className="px-4 offset-md-2 my-2 text-center pokemon-sub-detail">
                            {pokemon!=null ? pokemon.moves.map((v,i) => (
                                <Badge className="mx-2 mb-2" pill color="success">{words(v.move.name)}</Badge>
                            ) ) : <Skeleton count={4} />}
                        </Col>
                    )
            default:
                break
        }
    }
    catchPokemon = async(nick_name: string) => {
        setTimeout(() => {
            let {pokemon} = this.state
            pokemon.nick_name = nick_name
            let {dispatch} = this.props
            dispatch(addPokemon(pokemon))
            let word = `${this.state.pokemon?.name} was catched`
            this.setState({catching: false}, ()=> this.popupAlert.current.open(word))
        }, 1000)
    }
    render() {
        return (
            <Template {...this.props} noTab title={words(this.state.pokemon?.name)}>
                <Col md="12" className="mt-3">
                    <Link href="/">
                        {this.state.pokemon==null ? <Skeleton/> : <a className="back-button"><FaChevronLeft/> {words(this.state.pokemon?.name)} </a>}
                    </Link>
                </Col>
                <Col xs="8" md="4" className="p-4 offset-2 offset-md-4">
                    {this.state.pokemon==null ? <Skeleton height={this.state.imgHeight} /> : <img style={{minHeight: this.state.imgHeight}} className="img-fluid w-100" alt={`icon-pokemon-${this.state.pokemon.name}`} src={this.state.pokemon.sprites.front_default}/>}
                    <div className="text-center">
                        {this.state.pokemon==null ? <Skeleton/> : (
                            this.state.pokemon.types.map((v,i) => <Badge className="mx-2" key={i} color={v.type.name} pill>{words(v.type.name)}</Badge> )
                        ) }
                    </div>
                </Col>
                <Col md="12" className="my-2">
                    <PokemonTab viewOn={this.state.viewOn} changeView={(v: any)=>{
                        this.setState({viewOn: v})
                    }} pokemonName={this.props.query.name}/>                
                </Col>
                {this.renderPartial()}
                <div className="catch-now">
                    <button onClick={()=> this.setState({ catching: true }, () => {
                        setTimeout(() => {
                            this.setState({catching: false}, () => {
                                if (timeToCatch()) {
                                    this.popupPrompt.current.open("Enter pokemon nickname")
                                } else {
                                    this.popupAlert.current.open(`${words(this.state.pokemon?.name)} failed to catch :( `)
                                }
                            })
                        }, 5000)
                    })} 
                    className="btn btn-base btn-red">
                        <PlainPokeball size={20} /> <span className="ml-2 text-bold">Catch Now</span>
                    </button>
                </div>
                {this.state.catching && <CatchingPokemon  />}
                <Alert ref={this.popupAlert} />
                <Alert validatePropmt={(feedback: string): (string|null) => {
                    if (isNickNameExist(feedback)) {
                        return "Username already picked"
                    }
                    return null
                }} type="prompt" submitPrompt={(feeback: string) => {
                    this.setState({catching: true}, () => {
                        setTimeout(() => {
                            this.catchPokemon(feeback)
                        }, 1000)
                    })
                }} ref={this.popupPrompt} />
            </Template>
        )
    }
}

export default withHoc(PokemonDetail)

