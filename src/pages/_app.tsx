import "bootstrap/dist/css/bootstrap.min.css"
import '../styles/styles.scss'

import Router from 'next/router';
import nProgress from 'nprogress'
import App, {AppInitialProps, AppContext} from 'next/app'
import { wrapper } from "redux/store";

Router.events.on('routeChangeStart', url => {
    nProgress.start()
    // initMainPage()
})
Router.events.on('routeChangeComplete', () => nProgress.done())
Router.events.on('routeChangeError', () => nProgress.done())


class MyApp extends App<AppInitialProps> 
{
    public static getInitialProps = async ({Component, ctx}: AppContext) => { 
        return {
            pageProps: {
                ...(Component.getInitialProps ? await Component.getInitialProps(ctx) : {}),
                pathname: ctx.pathname,
                query: ctx.query,
            },
        }
    }
   
    public render() {
        const {Component, pageProps} = this.props

        return (
            <Component {...pageProps} />
        )
    }
}


export default wrapper.withRedux(MyApp)
