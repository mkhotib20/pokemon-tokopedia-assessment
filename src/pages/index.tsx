import SinglePokemon from 'components/general/SinglePokemon'
import Template from 'components/Template'
import { GqlQuery, PageProps,Pokemon,PokemonsResponse } from 'interfaces'
import { getter } from 'network'
import  * as gql from 'gql-query-builder'
import React, { Component } from 'react'
import { Col, Row, Spinner } from 'reactstrap'
import {  isBottom } from 'utils'
import { withHoc } from 'utils/hoc'


interface IndexState {
	page: number
	pokemons: Pokemon[]
	isFetching: boolean
}

export class IndexPage extends Component<PageProps, IndexState> {
	constructor(props:PageProps) {
		super(props)
		this.state = {
			isFetching: true,
			page: 1,
			pokemons: [null, null, null, null, null, null, null, null],
		}
	}
	dataSize = () =>{
		return window.innerWidth <768 ? 9 : 11
	}
	query = (): GqlQuery => {
		let offset = (this.state.page-1)* this.dataSize()
		const query = gql.query({
			operation: 'pokemons',
			variables: {
				limit: {value: this.dataSize(), required: false},
				offset: offset
			},
			fields: [
				{
					results: ["name", "id", "image"]
				}
			]
		})		
		return query
	}
	async componentDidMount(){
		this.getPokemons(true)
	}
	componentWillUnmount() {
		document.removeEventListener('scroll', this.trackScrolling);
	}
	changeElement = () =>{

	}
	getPokemons = async (force?: boolean) => {
		let {pokemons} = this.state
		let {data, error} = await getter<PokemonsResponse>(this.query())
		if (error!=null) {
			console.log(error);
		} else {
			let dataPokemon = force ? data.pokemons.results : pokemons.concat(data.pokemons.results)
			this.setState({pokemons: dataPokemon, isFetching: false},()=>{
				document.addEventListener('scroll', this.trackScrolling);
			})
		}
	}
	trackScrolling = () => {
		if (isBottom()) {
		  this.setState({
			  page: this.state.page+1,
			  isFetching: true,
		  }, this.getPokemons)
		  document.removeEventListener('scroll', this.trackScrolling);
		}
	}
	render() {
		return (
			<Template {...this.props}>
				<Col md="12" className="my-3">
					<Row>
					{this.state.pokemons.map((v,i) => {
						return(
							<Col key={i} xs="6" md="3">
								<SinglePokemon useCount data={v} />
							</Col>
						)
					})}
					</Row>
				</Col>
				<Col md="12" className="text-center my-4">
				{this.state.isFetching && (
					<div className="text-center  mb-3">
						<Spinner color="warning" />
					</div>
				)}
				</Col>
			</Template>
		)
	}
}

export default withHoc(IndexPage)
