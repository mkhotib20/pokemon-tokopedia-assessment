import { GqlQuery, Response } from "interfaces"
import _axios from "./config"


export async function getter<T> (query: GqlQuery, args?: string[], key?: string) : Promise<Response<T>> {
    let result: Response<T> =  {
        error: null,
        data: null
    }
    try {
        let rsp = await _axios.post("",query)
        let data: any = rsp.data
        result.data = key ? (typeof data.data[key] != "undefined" ? data.data[key] : data.data) : data.data
        return result
    } catch (error) {
        result.error = error
        return result
    }
}