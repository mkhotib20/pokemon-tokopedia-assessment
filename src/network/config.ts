import axios from 'axios'

const _axios = axios.create({
    baseURL: "https://graphql-pokeapi.vercel.app/api/graphql",
})

export default _axios