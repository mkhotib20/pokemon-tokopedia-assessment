export interface Ability {
    ability: BaseName
    is_hidden: Boolean
    slot: number
  }
  

export interface BaseList {
    count: number
    next: string
    previous: string
    results: [BaseName]
    status: Boolean
    message: string
}

export interface BaseName {
    url: string
    name: string
}

export interface BaseResponse {
    message: string
    status: Boolean
    response: JSON
}


export interface GameIndex {
    game_index: number
    version: BaseName
}

export interface HeldItem {
    item: BaseName
    version_details: [VersionDetail]
}


export interface ListResponse {
    count: number
    next: string
    previous: string
    results: [JSON]
    status: Boolean
    message: string
}

export interface Move {
    move: BaseName
    version_group_details: [VersionGroupDetail]
}

export interface Pokemon {
    abilities: [Ability]
    base_experience: number
    forms: [BaseName]
    game_indices: [GameIndex]
    height: number
    held_items: [HeldItem]
    id: number
    is_default: Boolean
    location_area_encounters: string
    moves: [Move]
    name: string
    order: number
    species: BaseName
    sprites: Sprite
    stats: [Stat]
    types: [Type]
    weight: number
    status: Boolean
    message: string
    nick_name?: string
}

export interface PokemonItem {
    url: string
    name: string
    image: string
    id: number
}

export interface PokemonList {
    count: number
    next: string
    previous: string
    results: [PokemonItem]
    status: Boolean
    message: string
}

export interface Query {
    abilities: BaseList
    ability(ability: string): BaseResponse
    berries: BaseList
    berry(berry: string): BaseResponse
    eggGroups: BaseList
    eggGroup(eggGroup: string): BaseResponse
    encounterMethods: BaseList
    encounterMethod(encounterMethod: string): BaseResponse
    evolutionChains: BaseList
    evolutionChain(id: string): BaseResponse
    evolutionTriggers: BaseList
    evolutionTrigger(name: string): BaseResponse
    genders: BaseList
    gender(gender: string): BaseResponse
    growthRates: BaseList
    growthRate(growthRate: string): BaseResponse
    locations: BaseList
    location(location: string): BaseResponse
    moves: BaseList
    move(move: string): BaseResponse
    natures: BaseList
    nature(nature: string): BaseResponse
    pokemons(limit: number, offset: number): PokemonList
    pokemon(name: string): Pokemon
    regions: BaseList
    region(region: string): BaseResponse
    species: BaseList
    type: BaseList
}

export interface Sprite {
    back_default: string
    back_female: string
    back_shiny: string
    back_shiny_female: string
    front_default: string
    front_female: string
    front_shiny: string
    front_shiny_female: string
}

export interface Stat {
    base_stat: number
    effort: number
    stat: BaseName
}

export interface Type {
    slot: number
    type: BaseName
}



export interface VersionDetail {
    rarity: number
    version: BaseName
}

export interface VersionGroupDetail {
    level_learned_at: number
    move_learn_method: BaseName
    version_group: BaseName
}