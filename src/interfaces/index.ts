import { Router } from "next/router"
import { Dispatch } from "react";
import { AnyAction, Store } from "redux";
import { State } from "redux/reducer";

export interface PageProps extends State{
    router: Router
    query?: AvailableQuery
    dispatch?: Dispatch<AnyAction>
}

export interface AvailableQuery {
    name?: string
}

export interface TabProps {
    label: string
    active?: boolean
    href: string
}

export interface Pokemon {
    id: number
    name: string
    nick_name: string
    image: string
}

export interface PokemonsResponse{
    pokemons: {
        count: number
        results: Array<Pokemon>
    }
}

export interface PokemonResponse{
    pokemons: {
        count: number
        results: Array<Pokemon>
    }
}

export interface Response<T>{
    data: T | null
    error: any | null
}

export interface GqlQuery {
    variables: any
    query: string
}

