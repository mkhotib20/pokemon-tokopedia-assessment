import { Pokemon } from "interfaces/graphql"

const POKEMON_KEY = "POKEMON"
const POKEMON_META = "POKEMON_META_KEY"

export const isNickNameExist = (nickname: string): boolean=>{
    let pokedex = getFromStorage()
    let idx = pokedex.findIndex(e => e.nick_name.toLowerCase()==nickname.toLowerCase())
    return idx>=0
}

export const saveToStorage = (pokemons: Pokemon[]) =>{
    let saved = JSON.stringify(pokemons)
    localStorage.setItem(POKEMON_KEY, saved)
}

export const saveMetaCount = (pokemons: any): object =>{
    let old = localStorage.getItem(POKEMON_META)
    try {
        let toBeSaved = pokemons
        if (old && typeof old!="undefined" && old!=null) {
            let merged =  Object.assign(JSON.parse(old), pokemons)
            toBeSaved = merged
        }
        localStorage.setItem(POKEMON_META, JSON.stringify(toBeSaved))
        return toBeSaved
    } catch (error) {
        return {}
    }
}

export const getFromStorage = (): Pokemon[] =>{
    if (typeof window != "undefined") {
        let strData = localStorage.getItem(POKEMON_KEY)
        if (strData && strData!=null && typeof strData != "undefined") {
            try {
                let pokemon = JSON.parse(strData)
                return pokemon
            } catch (error) {
                return []
            }
        } 
    }
    console.warn("Only work client side");
    return []
}