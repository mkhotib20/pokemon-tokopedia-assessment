import { PageProps } from "interfaces"
import { withRouter } from "next/router"
import { Component } from "react"
import { connect } from "react-redux"
import { addPokemon, directPokemonCount } from "redux/actionCreator"
import { getFromStorage } from "./pokemon"

export const withHoc = (Wrapped: any) =>{
    class Wrapper extends Component<PageProps,any>
    {
        componentDidMount(){
            let {dispatch} = this.props
            let poke = getFromStorage()
            dispatch(addPokemon(poke))
            dispatch(directPokemonCount(poke.length))
        }
        render() {
            return (
                <Wrapped {...this.props} />
            )
        }
    }
    return connect(state => state)(withRouter(Wrapper))
}