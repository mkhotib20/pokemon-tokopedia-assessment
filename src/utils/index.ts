import moment from 'moment'

export function queryMaker (payload: string[],resource: string, offset: number, dataSize?: number): string{
    let $query = `
    query ${resource}($limit: Int=${dataSize}, $offset: Int=${offset}) {
        ${resource}(limit: $limit, offset: $offset) {
          count
          results {
            ${payload.map(v => v+"\n")}
          }
        }
    }`;
    return $query
}

export const isBottom = (): boolean => {
    if (typeof window!="undefined") {        
        return Math.ceil((window.innerHeight + window.scrollY)) >= document.body.offsetHeight
    } else {
        console.warn("Browser only");
        false
    }
}

export const tabs = [
    {label: "Pokemon List", href: "/", active: false},
    {label: "Pokedex", href: "/owned-pokemon", active: false},
]

export const words = (str: any): string => {
    if (typeof str == "string") {
        try {
            let arr = str.split("")
            let newArr = arr.map((s,i) => i==0 ? s.toUpperCase() : s)
            return newArr.join("").split("-").join(" ").split("_").join(" ")
        } catch (error) {
            return ''
        }
    } 
    return str
}

export const timeToCatch = (): boolean => {
    let sec = moment().format("s")
    let min = moment().format("m")
    let summed = parseInt(min)+parseInt(sec)
    return summed%2==0
}