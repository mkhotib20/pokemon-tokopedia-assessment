import { PageProps } from 'interfaces'
import React, { Component } from 'react'
import Head from 'next/head'
import {Col, Container, Row} from 'reactstrap'
import { Pokepedia } from './icons'
import Tab from './general/Tab'

interface TemplateProps extends PageProps {
    noTab?: boolean
    title?: string
}

class Template extends Component<TemplateProps, {
    tabShown: boolean
}> {
    constructor(props: TemplateProps) {
        super(props)
        this.state = {
            tabShown: false
        }
    }
    componentDidMount(){
        var lastScrollTop = 0;

		document.addEventListener("scroll", () => {
            var st = window.pageYOffset || document.documentElement.scrollTop; 
            if (st >= 83){
				this.setState({tabShown: true})
            } else {
				this.setState({tabShown: false})
				
			}
			lastScrollTop = st <= 0 ? 0 : st; 
		}, false);

    }
    render() {
        return (
            <Container>
                <Head>
                    <link rel="apple-touch-icon" sizes="180x180" href="/iconsets/apple-touch-icon.png" />
                    <link rel="icon" type="image/png" sizes="32x32" href="/iconsets/favicon-32x32.png" />
                    <link rel="icon" type="image/png" sizes="16x16" href="/iconsets/favicon-16x16.png" />
                    <link rel="manifest" href="/iconsets/site.webmanifest" />
                    <meta name="msapplication-TileColor" content="#da532c" />
                    <meta name="theme-color" content="#ffffff" />
                    <title>{this.props.title ? `${this.props.title} - ` : "" }Pokepedia</title>
                </Head>
                <Row>
                    {this.props.noTab || <>
                        <Col md="12" className="text-center my-4">
                            <Pokepedia/>
                        </Col>
                        <Col md="12" className="my-3">
                            <Tab router={this.props.router} />
                        </Col>
                        {this.state.tabShown && (
                            <div className="tab-scroled">
                                <Tab router={this.props.router} />
                            </div>
                        )}
                    </>}
                    {this.props.children}
                </Row>
            </Container>
        )
    }
}

export default Template
