import React, { Component } from 'react'
import Link from 'next/link'
import { Router } from 'next/router'
import { Badge } from 'reactstrap'
import { words } from 'utils'

interface PokemonTabs {
    viewOn: "moves" | "stats" | "overview"
    pokemonName: string
    changeView: any
}

const tabs = ["overview", "stats", "moves"]

export class PokemonTab extends Component<PokemonTabs> {

    isActive = (key: string): boolean => {
        return key == this.props.viewOn
    }
    render() {
        return (
            <div className="tab-bar pokemon-tab-bar">
                {tabs.map((v,i) => {
                    return(
                        <span onClick={()=>{
                            this.props.changeView(v)
                        }} tabIndex={1} className={`item ${this.isActive(v) ? 'active' : ''}`} >{words(v)}</span> 
                    )
                })}
                
            </div>
        )
    }
}

export default PokemonTab
