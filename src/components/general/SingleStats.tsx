import React from 'react'

const SingleStats = (props: {
    value: number
    label: string
}) => {
    return (
        <tr className="pokemon-stats">
            <td className="stats-label">{props.label}</td>
            <td className="pl-2 stats-label text-center text-red"><span>{props.value}</span></td>
            <td className="px-2" style={{width: "60%"}}>
                <div className="stats-bar">
                    <div className="stats" style={{width: `${props.value}%`}}></div>
                </div>
            </td>
        </tr>        
    )
}

export default SingleStats
