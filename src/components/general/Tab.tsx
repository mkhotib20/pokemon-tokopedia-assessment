import React, { Component } from 'react'
import Link from 'next/link'
import { Router } from 'next/router'
import { Badge } from 'reactstrap'
import { connect } from 'react-redux'
import { State } from 'redux/reducer'

interface Tabs extends State{
    router: Router
}

export class Tab extends Component<Tabs> {

    isActive = (url: string): boolean => {
        return url == this.props.router.pathname
    }
    render() {
        return (
            <div className="tab-bar">
               <Link href="/">
                    <a className={`item ${this.isActive("/") ? 'active' : ''}`} >Pokemon List</a> 
                </Link>
               <Link href="/owned-pokemon">
                    <a className={`item ${this.isActive("/owned-pokemon") ? 'active' : ''}`} >Pokedex <Badge color={this.isActive("/owned-pokemon") ?"light" : "red"} pill>{this.props.pokemonCount}</Badge> </a> 
                </Link>
            </div>
        )
    }
}

export default connect(state => state)(Tab)
