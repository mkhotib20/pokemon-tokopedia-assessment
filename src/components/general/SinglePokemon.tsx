import { PageProps, Pokemon } from 'interfaces'
import Link from 'next/link'
import React, { useEffect, useRef, useState } from 'react'
import Skeleton from 'react-loading-skeleton'
import { connect } from 'react-redux'
import { Badge } from 'reactstrap'
import { State } from 'redux/reducer'

interface SinglePokemonProps extends State {
    data: Pokemon
    onClick?: (data: Pokemon) => void
    useCount?: boolean
}

const SinglePokemon = (props: SinglePokemonProps) => {
    const [height, setHeight] = useState(10)
    const heightReference = useRef(null);
    useEffect(() => {
        if (heightReference!=null) {
            try {
                let height = heightReference.current.offsetWidth
                setHeight(height)
            } catch (error) {}
        }
    }, [])

    const getMeta = (name: string): number => {
        let {metaCount} = props
        if (metaCount) {
            return typeof metaCount[name] != "undefined" ? metaCount[name] : 0
        } 
        return 0
    }
    const content = () => {
        return(
            <>
                <div style={{minHeight: height}} className="pokemon-image">
                    {props.data!=null ? <img src={props.data.image} alt="Pokemon icon" />  : <Skeleton height={height}/> }
                </div>
                {props.data!=null ? (
                    <p>
                        {props.data.nick_name || props.data.name} <br/> 
                        {props.useCount ? (
                            <Badge color={getMeta(props.data.name) > 0 ? 'success' : 'secondary'} pill>
                                Owned : {getMeta(props.data.name)}
                            </Badge>
                        ) : <Badge color={'success'} pill>
                                {props.data.name}
                            </Badge>
                        }
                    </p>
                ) : <Skeleton/>}
            </>
        )
    }
    if (props.onClick) {
        return (
            <div onClick={()=>{
                props.onClick(props.data)
            }} className="single-pokemon">
                {content()}
            </div>
        )
    }
    return (
        <Link as={`/pokemon-detail/${props.data?.name}`} href={`/pokemon-detail/[name]`}>
            <a ref={heightReference} className="single-pokemon">
                {content()}
            </a>
        </Link>
    )
}

export default connect(state => state)(SinglePokemon)
