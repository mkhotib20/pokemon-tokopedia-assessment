import React, { Component, FormEvent } from 'react'
import { Button, Card, Col, FormFeedback, FormGroup, Input, Label, Row } from 'reactstrap'

class Alert extends Component<{
    submitPrompt?: any
    validatePropmt?: (feedbakc: string) => string | null
    type?: "alert" | "prompt"
    buttons?: Array<ExtraButton>
},{opened: boolean, msg: string, error: string | null}> {
    constructor(props: any) {
        super(props)
        this.state = {
            error: null,
            msg: "Hello",
            opened: false
        }
    }
    open = (msg: string) => this.setState({opened: true, msg: msg})
    close = () => this.setState({opened: false})

    renderAlert(){
        return (
            <div className="web-alert">
                <Card className="w-75 p-3">
                    <p style={{whiteSpace: "pre"}} className="text-dark">{this.state.msg}</p>
                    <Row>
                        <Col>
                            <Button onClick={this.close} block size="sm" color="light">Close</Button>
                        </Col>
                        {this.props.buttons && this.props.buttons.map((v,i) => {
                            return(
                                <Col key={i}>
                                    <Button onClick={v.onClick} block size="sm" color="light">{v.label}</Button>
                                </Col>
                            )
                        })}
                    </Row>
                </Card>    
            </div>
        )
    }
    submitPrompt = (e: FormEvent<HTMLFormElement>) => {
        e.stopPropagation()
        e.preventDefault()
        let fd = new FormData(e.currentTarget)
        let feedback = fd.get("feedback").toString()
        let isValid = true
        let validateResult = null
        if (this.props.validatePropmt) {
            validateResult = this.props.validatePropmt(feedback)
            isValid = validateResult==null
        }
        if (isValid) {
            this.props.submitPrompt(feedback)
            this.close()
        } else {
            this.setState({error: validateResult})
        }
    }
    renderPrompt(){
        return (
            <div className="web-alert">
                <Card className="w-75 p-3">
                    <form onSubmit={this.submitPrompt}>
                        <FormGroup>
                            <label className="text-dark">{this.state.msg}</label>
                            <Input invalid={this.state.error!=null} name="feedback" />
                            <FormFeedback>
                                {this.state.error}
                            </FormFeedback>
                        </FormGroup>
                        <Row>
                            <Col>
                                <Button type="reset" onClick={this.close} block size="sm" color="light">Close</Button>
                            </Col>
                            <Col>
                                <Button type="submit" block size="sm" color="success">Save</Button>
                            </Col>
                        </Row>
                    </form>
                </Card>    
            </div>
        )
    }

    render() {
        if (this.state.opened) {
            switch (this.props.type) {
                case "prompt":
                    return this.renderPrompt()            
                default:
                    return this.renderAlert()
                    break;
            }
        }
        return null
    }
}


export interface ExtraButton {
    onClick: () => void
    label: string
}

export default Alert
