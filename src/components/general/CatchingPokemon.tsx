import React from 'react'
import { ColorPokeball } from 'components/icons'

const CatchingPokemon = (props: {
    title?: string
}) => {
    return (
        <div className="catching">
            <ColorPokeball  className="rotating" size={90} />
            <h5 className="text-bold mt-4">{props.title || "Catching Pokemon . . . "}</h5>
        </div>
    )
}

export default CatchingPokemon
